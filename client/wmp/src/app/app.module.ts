import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Ng-bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// App Router
import { WmpRouter } from './app.routes';

// App Component
import { AuthService } from './shared/auth/auth.service';
import { SocketioService } from './shared/socket/socketio.service';

// App Component
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/navbar/login/login.component';
import { RegisterComponent } from './components/navbar/register/register.component';
import { SongsComponent } from './components/home/songs/songs.component';
import { SongComponent } from './components/home/songs/song/song.component';
import { SidebarComponent } from './components/home/sidebar/sidebar.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    SongsComponent,
    SongComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    WmpRouter,
    HttpClientModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  providers: [AuthService,SocketioService],
  bootstrap: [AppComponent]
})
export class AppModule { }

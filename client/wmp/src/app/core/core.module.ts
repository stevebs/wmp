import { NgModule } from '@angular/core'
import { WmpRouter } from '../app.routes';

import { HomeComponent } from '../components/home/home.component';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { FooterComponent } from '../components/footer/footer.component';
import { LoginComponent } from '../components/navbar/login/login.component';
import { RegisterComponent } from '../components/navbar/register/register.component';
import { SongsComponent } from '../components/home/songs/songs.component';
import { SongComponent } from '../components/home/songs/song/song.component';
import { SidebarComponent } from '../components/home/sidebar/sidebar.component';

@NgModule({
  declarations: [
    HomeComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    SongsComponent,
    SongComponent,
    SidebarComponent
  ],
  imports: [
    WmpRouter
  ],
  exports: [

  ]
})

export class CoreModule {}

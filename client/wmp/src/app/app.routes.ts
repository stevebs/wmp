import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';


const wmpRouter: Routes = [
  { path:'home', component: HomeComponent },
  { path: '**', component: HomeComponent}
];

export const WmpRouter = RouterModule.forRoot(wmpRouter, {useHash:false});

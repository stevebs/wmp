import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from '../../../shared/auth/auth.service';
import { LoginRegisterService } from '../loginRegisterService';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private loginRegisterService:LoginRegisterService) { }

  ngOnInit() {
    
  }

  openRegPopUp(content) {
    this.loginRegisterService.open(content);
  }

  onRegisterSubmit(regForm: NgForm) {
    this.loginRegisterService.register(regForm.value)
      .subscribe(
        (data: {}) => console.log(data),
        (error) => console.log(error)
      );
  }

}

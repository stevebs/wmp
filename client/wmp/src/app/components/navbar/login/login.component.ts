import { Component, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from '../../../shared/auth/auth.service';
import { LoginRegisterService } from '../loginRegisterService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  closeResult: string;

  constructor(private loginRegisterService:LoginRegisterService) { }

  ngOnInit() {

  }

  openLogPopUp(content) {
    this.loginRegisterService.open(content);
  }

  onLoginSubmit(logForm: NgForm) {
    this.loginRegisterService.login(logForm.value)
      .subscribe(
        (data: {}) => console.log(data),
        (error) => console.log(error)
      );
  }

}

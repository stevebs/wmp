import { Component, OnInit } from '@angular/core';
import { LoginRegisterService } from './loginRegisterService';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [LoginRegisterService]
})
export class NavbarComponent implements OnInit {

  public isCollapsed = false;
  private navLinks: string[] = ['Home','Songs', 'About'];

  constructor() { }

  ngOnInit() {
  }

}

import { Injectable, Inject } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

@Injectable()
export class LoginRegisterService {

  closeResult:string;

  constructor(private modalService: NgbModal, private http: HttpClient) { }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  login(logUser:{}) {
    return this.http.post('http://localhost:3000/api/login', logUser)
      .map(
        (response: Response) => {
          return response;
        }
      )
  }

  register(regUser:{}) {
    return this.http.post('http://localhost:3000/api/register', regUser)
      .map(
        (response: Response) => {
          return response;
        }
      )
  }

}

import { Component, OnInit } from '@angular/core';
import { SongService } from './songs.service';

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css'],
  providers: [SongService]
})

export class SongsComponent implements OnInit {

  songs:any;

  constructor(private songService: SongService) { }

  ngOnInit() {

  }

}

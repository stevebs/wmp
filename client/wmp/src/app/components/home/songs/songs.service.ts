import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SongService {

  constructor(private http:HttpClient) { }

  getSongs() {
    return this.http.get('http://localhost:3000/api/getSongs')
      .map(response => {
        return response;
      });
  }

}

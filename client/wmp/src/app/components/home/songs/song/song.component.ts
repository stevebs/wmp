import { Component, OnInit } from '@angular/core';
import { SongService } from '../songs.service';
import { SocketioService } from '../../../../shared/socket/socketio.service';

@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.css']
})
export class SongComponent implements OnInit {

  songs:any;

  constructor(private songService: SongService, private socketService: SocketioService) { }

  ngOnInit() {
    this.songService.getSongs().subscribe((response:any) => {
      this.songs = response.data;
    });
  }

  onPlaySong(song,audio) {
    this.socketService.playSong(song.replace('.mp3', ''),audio);
  }

  search(input) {
    this.socketService.searchBy(input);
  }

}

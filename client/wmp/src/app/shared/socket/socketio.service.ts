import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import * as SocketStream from 'socket.io-stream';



@Injectable()
export class SocketioService {

  private url:string = 'http://localhost:3000';
  private socket:any = null;
  private ss:any = null;

  constructor() {
    this.socket = io(this.url);
    this.ss = SocketStream;
  }

  playSong(songName,audioTag) {
    let stream = SocketStream.createStream();
    let parts = [];
    this.ss(this.socket).emit('streamSong', stream, {test:songName});
    stream.on('data', (chunk) => {
      parts.push(chunk);
    });
    stream.on('error', (err) => {
      console.log(err)
    });
    stream.on('end', () => {
      audioTag.src = URL.createObjectURL(new Blob(parts));
      audioTag.play();
    });
  }

  searchBy(input) {
    let stream = SocketStream.createStream();
    this.ss(this.socket).emit('search', stream, {type:'general',name:input});
    stream.on('data', (chunk) => {
      console.log(chunk)
    });
    stream.on('error', (err) => {
      console.log(err)
    });
    stream.on('end', () => {
      console.log('finish');
    });
  }

}

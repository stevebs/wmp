var ss = require('socket.io-stream'),
    fs = require('fs'),
    path = require('path'),
    db = require('../../database/init');

module.exports = function (server) {

    var io = require('socket.io')(server);
    var connected = {};
    var data = {};
    var mwSocket = function (socket, next) {
        next();
    };
    var getNames = function () {
        var names = [];
        for (var socket in connected) {
            names.push(connected[socket]);
        }
        return names;
    };

    io.use(mwSocket);
    io.on('connection', function (socket) {

      connected[socket.id] = socket.id;

      ss(socket).on('streamSong', (stream, data) => {
        let filename = path.basename('Ed_Sheeran.mp3');
        fs.createReadStream(__dirname + '../../../public/'+filename).pipe(stream);

      });

      ss(socket).on('search', (stream, query) => {
        let type = query.type;
        console.log(query);
        db.searchByType(type,query.name, (err,res) => {
          if(err) {
            console.log(err);
            // ss(socket).emit('error');
          } else {
            let resJsonStream = ss.createStream(res[0]);
            console.log(res);
            // resJsonStream.pipe(stream);
          }
        });
      });

        socket.on('streamSongReq',function(data){
          console.log("dsads");
          var filename = 'Ed_Sheeran.mp3';
          socket.emit('testemit', {data:filename});
          // var stream = ss.createStream();
          // stream(socket).emit('streamSong', stream,{name:filename});
          // fs.createBlobReadStream(filename).pipe(stream);
          // var audio = document.getElementById('player');
          // ss(socket).on('audio-stream', function(stream, data) {
          //     parts = [];
          //     stream.on('data', function(chunk){
          //         parts.push(chunk);
          //     });
          //     stream.on('end', function () {
          //         audio.src = (window.URL || window.webkitURL).createObjectURL(new Blob(parts));
          //         audio.play();
          //     });
          // });
        });

        socket.on('disconnect', function () {
            console.log(connected[socket.id]+" was deleted")
            delete connected[socket.id];
            // data.connected = getNames();
        });

    });
  }

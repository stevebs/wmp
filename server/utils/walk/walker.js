var walk = require('walk'),
    fs = require('fs'),
    db = require('../../database/init'),
    walker,
    path,
    walkObj = {};


walkObj.search = function(srcPath) {
  var list = [];

  walker = walk.walk(srcPath, {});

  walker.on("file", function (root, fileStats, next) {
    fs.readFile(fileStats.name, function () {
      list.push(fileStats);
      db.saveFileStats(...list, (err,result) => {
        if(err) throw err;
        console.log(result);
        next();
      })

    });
  });

  walker.on("errors", function (root, nodeStatsArray, next) {
    return null;
    next();
  });

  walker.on("end", function () {
    console.log("all done");
    // console.log(...list);
    return list;
  });

}

module.exports = walkObj;

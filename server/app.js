var bodyParser = require("body-parser"),
    express = require("express"),
    path = require("path"),
    fs = require("fs"),
    cors = require('cors'),
    morgan = require('morgan'),
    io = require('./utils/socket/socketio'),
    config = require('./utils/config/init'),
    walker = require('./utils/walk/walker'),
    db = require('./database/init'),
    em = require('./utils/event/emitter'),
    app = express(),
    router = express.Router(),
    port = process.env.PORT || 3000,
    appRoute = require('./route/api')(router);

//bootstrap
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(morgan('combined'))
app.use(express.static(path.join(__dirname, 'public')));
app.disable('x-powered-by');

//route
app.use('/api', appRoute);

//listener
em.on('error', () => {
  console.log('an error occurred!');
});

em.once('startServer',() => {
  io(app.listen(port, () => {
  console.log('Magic happens on port ' + port);
  em.emit('startDatabase');
  // walker.search(__dirname+'\\public\\');
  }));
});

em.on('startDatabase', () => {
  db.createDatabase();
});

//start
em.emit('startServer');

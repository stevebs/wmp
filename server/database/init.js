var config = require('../utils/config/config'),
    fs = require('fs'),
    db = {},
    choice = 'nedb',
    mongo,
    nedb;

  db.createDatabase = () => {
    if(choice == 'nedb'){
      nedb = require('./nedb')(config.nedb.path);
      return;
    }
    if (choice == 'mongo') {
      mongo = require('./mongo')(config.mongo.uri);
      return;
    }
  }

  db.saveFileStats = (stats,cb) => {
    nedb.insert(stats, (err,list) => {
      if(err) return cb(err);
      return cb(null,list);
    });
  }

  db.loginUser = (logUser,cb) => {
    nedb.findOne({email:logUser.email}, (err,user) => {
      if(err) return cb('error');
      cb(null,user);
    });
  }

  db.resgisterUser = (regUser,cb) => {
    nedb.insert(regUser, (err,user) => {
      if(err) return cb('error');
      cb(null,user);
    });
  }

  db.createData = (data) => {
    if(choice == 'nedb'){
      nedb.insert(data, (err,res) => {
        if (err) {
          console.log(err);
        } else {
          return res;
        }
      });
    }
    if (choice == 'mongo') {
    }
  }

  db.readData = (cb) => {
    if(choice == 'nedb'){
      nedb.find({type:'file'}, (err,res) => {
        if (err) return cb(err);
        cb(null,res)
      });
    }
    if (choice == 'mongo') {
    }
  }

  db.searchByType = (type,input,cb) => {
    if (type === 'general') {
      nedb.find({username:input}, (err,res) => {
        if(err) return cb(err);
        cb(null,res);
      })
    }

  }

  db.updateData = (id) => {
    if(choice == 'nedb'){
      nedb.update({_id:id}, (err,res) => {
        if (err) {
          console.log(err);
        } else {
          return res;
        }
      });
    }
    if (choice == 'mongo') {
    }
  }

  db.deleteData = (id) => {
    if(choice == 'nedb'){
      nedb.remove({_id:id}, (err,res) => {
        if (err) {
          console.log(err);
        } else {
          return res;
        }
      });
    }
    if (choice == 'mongo') {
    }
  }

module.exports = db;

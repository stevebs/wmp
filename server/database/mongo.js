const mongoose = require('mongoose'),
      db = {};

module.exports = function(uri) {

  mongoose.connect(uri);

  // When successfully connected
  mongoose.connection.on('connected', function () {
    console.log('Connection open to DB');
  });

  // If the connection throws an error
  mongoose.connection.on('error',function (err) {
    console.log('Mongoose default connection error: ' + err);
  });

  // When the connection is disconnected
  mongoose.connection.on('disconnected', function () {
    console.log('DB disconnected');
  });

  // If the Node process ends, close the Mongoose connection
  process.on('SIGINT', function() {
    mongoose.connection.close(function () {
      console.log('Mongoose default connection disconnected through app termination');
      process.exit(0);
    });
  });

}

var Datastore = require('nedb'),
    db = {};


module.exports = (path) => {
  db = new Datastore({ filename: path });

  db.loadDatabase((err) => {
    // Now commands will be executed
    if (err) {
      console.log(err);
    } else {
      console.log("success");
    }
  });

  return db;

}

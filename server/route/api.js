// var walk = require('../utils/walk/walker');
var db = require('../database/init');

module.exports =  function(router) {

  router.route('/login')
    .post((req,res) => {
      let logUser = {
        email: req.body.email,
        password: req.body.password
      };
      if(logUser.email === "" || logUser.email === undefined || logUser.password === "" || logUser.password === undefined) {
        return res.json({success:false, data: 'Must provide email and password'});
      }
      db.loginUser(logUser, (err,dbUser) => {
        if(err) return res.json({success:false, data: 'No user with this email ' + logUser.email});
        if(logUser.password !== dbUser.password) return res.json({success:false, data: 'Email or Password are not match'});
        res.json({success:true, data: dbUser});
      });
    })

  router.route('/register')
    .post((req,res) => {
      let regUser = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        passwordCon: req.body.passwordCon
      };
      if(regUser.username === "" || regUser.username === undefined || regUser.email === "" || regUser.email === undefined
       || regUser.password === "" || regUser.password === undefined || regUser.passwordCon === "" || regUser.passwordCon === undefined) {
         return res.json({success:false, data: 'Must provide all required fields'});
      }
      if(regUser.password !== regUser.passwordCon) return res.json({success:false, data: 'Password not match'});
      db.resgisterUser(regUser, (err,response) => {
        if(err) return res.json({success:false, data: 'Error ecoured'});
        res.json({success:true, data: response});
      })
    })

  router.route('/getSongs')
    .get((req, res) => {
      db.readData((err,songs) => {
        if(err) throw err;
        console.log(songs);
        res.json({success:true, data:songs});
      })
    })
    .post((req, res) => {
      res.json({msg:'post Songs'});
    })

  router.route('/addPlaylist')
    .get((req, res) => {
      res.json({msg:"get addPlaylist"});
    })
    .post((req, res) => {
      res.json({msg:"post addPlaylist"});
    })

  router.route('/search')
    .post((req, res) => {
      res.json({msg:"post search"})
      // console.log(req.body);
      // walk.search(req.body);
      // res.json({msg:"hry"})
    })

  return router;
}
